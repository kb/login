User Login
**********

Django Application for user login and logout etc.

Install
=======

Virtual Environment
-------------------

::

  virtualenv --python=python3 venv-login
  # or
  python3 -m venv venv-login

  source venv-login/bin/activate

  pip install -r requirements/local.txt

Testing
-------

::

  find . -name '*.pyc' -delete
  pytest --color=yes --tb=short --show-capture=no -x

Usage
-----

::

  ./init_dev.sh

You can log in with user ``staff``, password ``letmein``...

To test the password reset, use web@pkimber.net

Release
=======

https://www.kbsoftware.co.uk/docs/
