# -*- encoding: utf-8 -*-
import logging

from django.contrib.auth import get_user_model
from django.contrib.auth.backends import ModelBackend


logger = logging.getLogger(__name__)
UserModel = get_user_model()


class ExistingRemoteUserBackend(ModelBackend):
    """Remote user backend which does not create unknown users.

    This is a copy of ``auth.backends.RemoteUserBackend`` with updated:

    1. ``authenticate``, case-insensitive lookup of ``username``
    2. ``authenticate``, does not automatically create an unknown user.
    2. ``clean_username``, format domain and username as ``domain@username``

    Ideas for case-insensitive lookup copied from,
    How to Implement Case-Insensitive Username:
    https://simpleisbetterthancomplex.com/tutorial/2017/02/06/how-to-implement-case-insensitive-username.html

    User / domain names to be lower case:
    https://www.kbsoftware.co.uk/crm/ticket/3681/

    """

    def _get_user_ignore_case(self, username):
        username_ignore_case = "{}__iexact".format(UserModel.USERNAME_FIELD)
        user = UserModel._default_manager.get(
            **{username_ignore_case: username}
        )
        return user

    def authenticate(self, request, remote_user):
        if not remote_user:
            return
        user = None
        username = self.clean_username(remote_user)
        try:
            user = self._get_user_ignore_case(username)
        except UserModel.DoesNotExist:
            logger.error(
                "Cannot authenticate '{}' (remote_user '{}')".format(
                    username, remote_user
                )
            )
        return user if self.user_can_authenticate(user) else None

    def clean_username(self, username):
        """Append the domain name to the 'username'.

        The original username is in this format::

          kbsoftware\\patrick

        Update the user names on the NMS system so we append the domain name to
        the username (with ``@`` as the separator) e.g::

          Test.User@EXTRANET-UK
          Alt@MyDomain

        We retrieve the actual ``username`` from the database because the
        ``RemoteUserMiddleware`` needs an exact match.

        https://www.kbsoftware.co.uk/crm/ticket/1610/

        """
        pos = username.find("\\")
        if pos == -1:
            result = username
        else:
            result = username[pos + 1 :] + "@" + username[:pos]
        try:
            user = self._get_user_ignore_case(result)
            result = user.username
        except UserModel.DoesNotExist:
            pass
        return result

    def configure_user(self, user):
        return user
