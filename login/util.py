# -*- encoding: utf-8 -*-
from django.contrib.auth import get_user_model
from .models import SYSTEM_GENERATED


def get_system_generated_user():
    try:
        sg_user = get_user_model().objects.get(username=SYSTEM_GENERATED)
    except get_user_model().DoesNotExist:
        # If no password is provided, set_unusable_password() will be called
        sg_user = get_user_model().objects.create_user(SYSTEM_GENERATED)
    return sg_user
