# -*- encoding: utf-8 -*-
from rest_framework import serializers
from login.service import KBSoftwareOIDCAuthenticationBackend


class AuthTokenOnCodeSerializer(serializers.Serializer):
    """

    Uses:

    - ``KBSoftwareOIDCAuthenticationBackend`` (``login.service.py)``
    - ``ObtainAuthTokenOnCode`` (``login.api.py)``

    """

    code = serializers.CharField()

    def validate(self, attrs):
        code = attrs.get("code")
        if code:
            x = KBSoftwareOIDCAuthenticationBackend()
            user = x.authenticate_ember(code)
            if not user:
                msg = "Unable to log in"
                raise serializers.ValidationError(msg, code="authorization")
        else:
            msg = "Must include 'code'"
            raise serializers.ValidationError(msg, code="authorization")
        attrs["user"] = user
        return attrs
