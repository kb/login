# -*- encoding: utf-8 -*-
import pytest
import pytz

from datetime import datetime
from django.conf import settings
from django.contrib.auth.tokens import PasswordResetTokenGenerator
from django.urls import reverse
from django.utils.encoding import force_bytes
from django.utils.http import urlsafe_base64_encode
from freezegun import freeze_time
from http import HTTPStatus

from login.models import LoginAudit
from .factories import TEST_PASSWORD, UserFactory


@pytest.mark.django_db
def test_is_auth(client):
    user = UserFactory()
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    response = client.get(reverse("login.is.auth"))
    assert HTTPStatus.OK == response.status_code


@pytest.mark.django_db
def test_is_auth_not_auth(client):
    response = client.get(reverse("login.is.auth"))
    assert HTTPStatus.FORBIDDEN == response.status_code


@pytest.mark.django_db
def test_is_auth_url(client):
    """The URL is hard-coded into the nGINX config, so must not change."""
    assert "/accounts/is/auth/" == reverse("login.is.auth")


@pytest.mark.django_db
def test_login(client):
    with freeze_time(datetime(2017, 5, 21, 21, 30, 0, tzinfo=pytz.utc)):
        response = client.get(reverse("login"))
        # add 'today' to the context
        assert "today" in response.context
        today = response.context["today"]
        assert datetime(2017, 5, 21, 21, 30, 0, tzinfo=pytz.utc) == today()
    assert HTTPStatus.OK == response.status_code
    assert "path" in response.context
    assert "/accounts/login/" == response.context["path"]
    assert "testing" in response.context
    assert settings.TESTING == response.context["testing"]
    assert "use_openid_connect" in response.context
    assert settings.USE_OPENID_CONNECT == response.context["use_openid_connect"]


@pytest.mark.django_db
def test_login_audit_list(client):
    LoginAudit.objects.create_login_audit(
        LoginAudit.EMAIL_DOES_NOT_EXIST, "web@pkimber.net"
    )
    LoginAudit.objects.create_login_audit(
        LoginAudit.MISSING_EMAIL, None, {"fruit": "Apple"}
    )
    user = UserFactory(is_staff=True, is_superuser=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    response = client.get(reverse("login.audit.list"))
    assert HTTPStatus.OK == response.status_code
    assert "loginaudit_list" in response.context
    qs = response.context["loginaudit_list"]
    assert 2 == qs.count()


@pytest.mark.django_db
def test_logout(client):
    user = UserFactory(username="web")
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    assert user.is_authenticated is True
    response = client.get(reverse("logout"))
    assert HTTPStatus.FOUND == response.status_code
    assert reverse("project.home") == response.url
    assert response.wsgi_request.user.is_authenticated is False


@pytest.mark.django_db
def test_password_change(client):
    user = UserFactory(username="web")
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    response = client.get(reverse("password_change"))
    assert HTTPStatus.OK == response.status_code
    assert "testing" in response.context
    assert settings.TESTING == response.context["testing"]


@pytest.mark.django_db
def test_password_change_post(client):
    user = UserFactory(username="web")
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    data = {
        "old_password": TEST_PASSWORD,
        "new_password1": "apple-orange",
        "new_password2": "apple-orange",
    }
    response = client.post(reverse("password_change"), data)
    assert HTTPStatus.FOUND == response.status_code, response.context[
        "form"
    ].errors
    assert reverse("password_change_done") == response.url


@pytest.mark.django_db
def test_password_change_post_password_matches_email(client):
    user = UserFactory(username="patrick", email="patrick@kbsoftware.co.uk")
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    data = {
        "old_password": TEST_PASSWORD,
        "new_password1": "kbsoftware",
        "new_password2": "kbsoftware",
    }
    response = client.post(reverse("password_change"), data)
    assert HTTPStatus.OK == response.status_code
    assert {
        "new_password2": ["The password is too similar to the email address."]
    } == response.context["form"].errors


@pytest.mark.django_db
def test_password_change_post_password_matches_username(client):
    user = UserFactory(username="patrickkimber")
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    data = {
        "old_password": TEST_PASSWORD,
        "new_password1": "patrickkimber",
        "new_password2": "patrickkimber",
    }
    response = client.post(reverse("password_change"), data)
    assert HTTPStatus.OK == response.status_code
    assert {
        "new_password2": ["The password is too similar to the username."]
    } == response.context["form"].errors


@pytest.mark.django_db
def test_password_change_post_password_too_short(client):
    user = UserFactory(username="web")
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    data = {
        "old_password": TEST_PASSWORD,
        "new_password1": "2short",
        "new_password2": "2short",
    }
    response = client.post(reverse("password_change"), data)
    assert HTTPStatus.OK == response.status_code
    assert {
        "new_password2": [
            (
                "This password is too short. "
                "It must contain at least 8 characters."
            )
        ]
    } == response.context["form"].errors


@pytest.mark.django_db
def test_password_change_post_wrong_old_password(client):
    user = UserFactory(username="web")
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    data = {
        "old_password": "wrong-old-password",
        "new_password1": "apple-orange",
        "new_password2": "apple-orange",
    }
    response = client.post(reverse("password_change"), data)
    assert HTTPStatus.OK == response.status_code
    assert {
        "old_password": [
            (
                "Your old password was entered incorrectly. "
                "Please enter it again."
            )
        ]
    } == response.context["form"].errors


@pytest.mark.django_db
def test_password_done(client):
    user = UserFactory(username="web")
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    response = client.get(reverse("password_change_done"))
    assert HTTPStatus.OK == response.status_code
    assert "testing" in response.context
    assert settings.TESTING == response.context["testing"]


@pytest.mark.django_db
def test_password_reset(client):
    response = client.get(reverse("password_reset"))
    assert HTTPStatus.OK == response.status_code


@pytest.mark.django_db
def test_password_reset_post(client):
    user = UserFactory(email="test@pkimber.net")
    response = client.post(reverse("password_reset"), {"email": user.email})
    assert HTTPStatus.FOUND == response.status_code, response.context[
        "form"
    ].errors


@pytest.mark.django_db
def test_password_reset_audit_report(client):
    user = UserFactory(is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    response = client.get(reverse("password_reset_audit_report"))
    assert HTTPStatus.OK == response.status_code
    assert "testing" in response.context
    assert settings.TESTING == response.context["testing"]


@pytest.mark.django_db
def test_password_reset_confirm(client):
    user = UserFactory()
    response = client.get(
        reverse(
            "password_reset_confirm",
            args=[
                str(urlsafe_base64_encode(force_bytes(user.pk))),
                PasswordResetTokenGenerator().make_token(user),
            ],
        )
    )
    assert HTTPStatus.FOUND == response.status_code
    # assert "abc" == response.url


@pytest.mark.django_db
def test_password_reset_complete(client):
    response = client.get(reverse("password_reset_complete"))
    assert HTTPStatus.OK == response.status_code
    assert "testing" in response.context
    assert settings.TESTING == response.context["testing"]


@pytest.mark.django_db
def test_password_reset_done(client):
    response = client.get(reverse("password_reset_done"))
    assert HTTPStatus.OK == response.status_code
    assert "testing" in response.context
    assert settings.TESTING == response.context["testing"]
