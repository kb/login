# -*- encoding: utf-8 -*-
import pytest

from datetime import datetime
from freezegun import freeze_time

from login.models import LoginAudit


@pytest.mark.django_db
def test_str():
    with freeze_time(datetime(2017, 5, 21, 19, 35)):
        x = LoginAudit.objects.create_login_audit(
            LoginAudit.MISSING_EMAIL, email="web@pkimber.net"
        )
    assert "{}. 21/05/2017 19:35 (no-email-in-payload) web@pkimber.net".format(
        x.pk
    ) == str(x)


@pytest.mark.django_db
def test_str_no_email():
    with freeze_time(datetime(2017, 5, 21, 19, 35)):
        x = LoginAudit.objects.create_login_audit(
            LoginAudit.MISSING_EMAIL, payload={"fruit": "orange"}
        )
    assert "{}. 21/05/2017 19:35 (no-email-in-payload) ['fruit']".format(
        x.pk
    ) == str(x)
