# -*- encoding: utf-8 -*-
import pytest

from django.conf import settings
from django.urls import reverse
from http import HTTPStatus
from urllib.parse import urlparse

from login.tests.factories import TEST_PASSWORD, UserFactory


def url_is_project_home(url):
    """When using Active Directory, we redirect to the 'project.no.auth' page.

    A failed login should redirect to ``project.no.auth``, not ``login``.
    For more information, search for ``REMOTE_USER``.

    """
    x = urlparse(url)
    return x.path == reverse("project.no.auth")


class PermTest:
    def __init__(self, client):
        setup_users()
        self.client = client

    def _no_login_anon(self, url):
        """Check anon user cannot login."""
        self.client.logout()
        response = self.client.get(url)
        assert response.status_code in [
            HTTPStatus.FORBIDDEN,
            HTTPStatus.FOUND,
            HTTPStatus.UNAUTHORIZED,
        ], response.status_code
        if response.status_code == HTTPStatus.FOUND:
            # PJK 23/05/2019, The ``invoice_download`` function was returning
            # ``admin:login` when testing the ``invoice`` app.
            # PJK, 29/06/2021, To stop this, remove the 'admin' app.
            urls_to_check = [reverse("login")]
            if settings.USE_OPENID_CONNECT:
                urls_to_check.append(reverse("oidc_authentication_init"))
            assert any([x in response.url for x in urls_to_check]), response.url

    def _no_login_staff(self, url):
        """Check staff user cannot login."""
        assert self.client.login(username="staff", password=TEST_PASSWORD)
        response = self.client.get(url)
        assert HTTPStatus.FOUND == response.status_code, response.status_code
        urls_to_check = [reverse("login")]
        if settings.USE_OPENID_CONNECT:
            urls_to_check.append(reverse("oidc_authentication_init"))
        assert any([x in response.url for x in urls_to_check]), response.url

    def _no_login_web(self, url):
        """Check web user cannot login."""
        assert self.client.login(username="web", password=TEST_PASSWORD) is True
        response = self.client.get(url)
        assert response.status_code in [
            HTTPStatus.FOUND,
            HTTPStatus.UNAUTHORIZED,
            HTTPStatus.FORBIDDEN,
        ], response.status_code
        if response.status_code == HTTPStatus.FOUND:
            # PJK 23/05/2019, The ``invoice_download`` function is returning
            # ``admin:login` when testing the ``invoice`` app.
            # PJK, 29/06/2021, To stop this, remove the 'admin' app.
            urls_to_check = [reverse("login")]
            if settings.USE_OPENID_CONNECT:
                urls_to_check.append(reverse("oidc_authentication_init"))
            assert any([x in response.url for x in urls_to_check]), response.url

    def admin(self, url):
        self._no_login_anon(url)
        self._no_login_web(url)
        self._no_login_staff(url)
        # check staff user can login
        assert self.client.login(username="admin", password=TEST_PASSWORD)
        response = self.client.get(url)
        assert HTTPStatus.OK == response.status_code

    def anon(self, url):
        self.client.logout()
        response = self.client.get(url)
        message = (
            "'url should be public '{}', status_code: {}, context: {}".format(
                url, response.status_code, response.context
            )
        )
        assert HTTPStatus.OK == response.status_code, message

    def auth(self, url, expect=None, expect_location=None):
        """Check a user who is logged in has access to the view.

        Keyword arguments:
        expect -- can be used when the expected status code is not HTTPStatus.OK
        expect_location -- can be used where the expected status code
        is HTTPStatus.FOUND and we want to verify the redirect.

        """
        if not expect:
            expect = HTTPStatus.OK
        # check a user who 'is_authenticated' can login
        self.client.logout()
        response = self.client.get(url)
        assert response.status_code in [
            HTTPStatus.FOUND,
            HTTPStatus.UNAUTHORIZED,
            HTTPStatus.FORBIDDEN,
        ], response.status_code
        if response.status_code == HTTPStatus.FOUND:
            assert reverse("login") in response.url
        # check web user can login
        self.client.logout()
        assert self.client.login(username="web", password=TEST_PASSWORD)
        response = self.client.get(url)
        message = "Expecting status code '{}', got '{}' for {}: {}".format(
            expect, response.status_code, url, response.get("Location", "")
        )
        assert expect == response.status_code, message
        # check staff user can login
        self.client.logout()
        assert self.client.login(username="staff", password=TEST_PASSWORD)
        response = self.client.get(url)
        assert expect == response.status_code
        if expect_location:
            assert expect_location == response.url

    def staff(self, url, expect=None, expect_location=None):
        if not expect:
            expect = HTTPStatus.OK
        self._no_login_anon(url)
        self._no_login_web(url)
        # check staff user can login
        assert self.client.login(username="staff", password=TEST_PASSWORD)
        response = self.client.get(url)
        assert expect == response.status_code, response
        if expect_location:
            assert expect_location == response.url

    def superuser(self, url):
        self._no_login_anon(url)
        self._no_login_web(url)
        self._no_login_staff(url)
        # check superuser can login
        assert self.client.login(username="superuser", password=TEST_PASSWORD)
        response = self.client.get(url)
        assert HTTPStatus.OK == response.status_code, response


class PermTestRemote:
    """For testing Active Directory login (using 'REMOTE_USER').

    A failed login should redirect to ``project.home``, not ``login``.

    """

    def __init__(self, client):
        setup_users()
        self.client = client

    def _no_login_anon(self, url):
        """Check anon user cannot login."""
        self.client.logout()
        response = self.client.get(url)
        assert response.status_code in [
            HTTPStatus.FOUND,
            HTTPStatus.UNAUTHORIZED,
        ], response.status_code
        if response.status_code == HTTPStatus.FOUND:
            assert url_is_project_home(response.url) is True, response.url

    def _no_login_staff(self, url):
        """Check staff user cannot login."""
        response = self.client.get(url, REMOTE_USER="staff")
        assert HTTPStatus.FOUND == response.status_code
        assert url_is_project_home(response.url) is True

    def _no_login_web(self, url):
        """Check web user cannot login."""
        response = self.client.get(url, REMOTE_USER="web")
        assert response.status_code in [
            HTTPStatus.FOUND,
            HTTPStatus.UNAUTHORIZED,
            HTTPStatus.FORBIDDEN,
        ], response.status_code
        if response.status_code == HTTPStatus.FOUND:
            assert url_is_project_home(response.url) is True

    def admin(self, url):
        self._no_login_anon(url)
        self._no_login_web(url)
        self._no_login_staff(url)
        # check admin user can login
        response = self.client.get(url, REMOTE_USER="admin")
        assert HTTPStatus.OK == response.status_code

    def auth(self, url, expect=None, expect_location=None):
        if not expect:
            expect = HTTPStatus.OK
        # check a user who 'is_authenticated' can login
        self.client.logout()
        response = self.client.get(url)
        assert HTTPStatus.FOUND == response.status_code, response.status_code
        assert url_is_project_home(response.url) is True, response.url
        # check web user can login
        self.client.logout()
        response = self.client.get(url, REMOTE_USER="web")
        message = "Expecting status code '{}', got '{}' for {}: {}".format(
            expect, response.status_code, url, response.get("Location", "")
        )
        assert expect == response.status_code, message
        # check staff user can login
        self.client.logout()
        response = self.client.get(url, REMOTE_USER="staff")
        assert expect == response.status_code
        if expect_location:
            assert expect_location == response.url

    def staff(self, url):
        self._no_login_anon(url)
        self._no_login_web(url)
        # check staff user can login
        response = self.client.get(url, REMOTE_USER="staff")
        assert HTTPStatus.OK == response.status_code, response

    def superuser(self, url):
        self._no_login_anon(url)
        self._no_login_web(url)
        self._no_login_staff(url)
        # check superuser can login
        response = self.client.get(url, REMOTE_USER="superuser")
        assert HTTPStatus.OK == response.status_code, response


@pytest.fixture
def perm_check(client):
    """Check permissions on a URL.

    We use a clever trick to pass parameters to the fixture.  For details:
    py.test: Pass a parameter to a fixture function
    http://stackoverflow.com/questions/18011902/py-test-pass-a-parameter-to-a-fixture-function

    To use this fixture::

      from login.tests.fixture import perm_check

      @pytest.mark.django_db
      def test_list(perm_check):
          url = reverse('enquiry.list')
          perm_check.staff(url)

    """
    return PermTest(client)


@pytest.fixture
def perm_check_remote(client):
    """

    To use this fixture::

      from login.tests.fixture import perm_check_remote

    """
    return PermTestRemote(client)


def setup_users():
    """Using factories - set-up users for permissions test cases."""
    UserFactory(
        username="admin",
        email="admin@pkimber.net",
        is_staff=True,
        is_superuser=True,
    )
    UserFactory(username="staff", email="staff@pkimber.net", is_staff=True)
    UserFactory(
        username="superuser",
        email="superuser@pkimber.net",
        is_staff=True,
        is_superuser=True,
    )
    UserFactory(
        username="web",
        email="web@pkimber.net",
        first_name="William",
        last_name="Webber",
        is_staff=False,
        is_superuser=False,
    )
