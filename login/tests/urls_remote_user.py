# -*- encoding: utf-8 -*-
from django.http import HttpResponse
from django.template import RequestContext, Template
from django.urls import include, re_path
from django.views.decorators.cache import never_cache
from django.views.generic import TemplateView

from example_login.views import NoAuthView, TestView
from login.views import RegisterCreateView


@never_cache
def remote_user_auth_view(request):
    """Dummy view for remote user tests."""
    t = Template("Username is {{ user }}.")
    c = RequestContext(request, {})
    return HttpResponse(t.render(c))


urlpatterns = [
    re_path(r"^", view=include("login.urls")),
    re_path(r"^$", view=NoAuthView.as_view(), name="project.no.auth"),
    re_path(r"^remote_user/$", view=remote_user_auth_view),
    # extra urls
    re_path(r"^testing/home/$", view=NoAuthView.as_view(), name="project.home"),
    re_path(
        r"^testing/accounts/register/$",
        view=RegisterCreateView.as_view(),
        name="register",
    ),
    re_path(
        r"^testing/dash/$",
        view=TemplateView.as_view(template_name="example/dash.html"),
        name="project.dash",
    ),
    re_path(
        r"^testing/settings/$",
        view=TemplateView.as_view(template_name="example/dash.html"),
        name="project.settings",
    ),
    re_path(r"^testing/test/$", view=TestView.as_view(), name="example.test"),
]
