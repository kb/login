# -*- encoding: utf-8 -*-
import pytest

from datetime import datetime
from django.contrib.auth import get_user_model
from freezegun import freeze_time

from login.models import LoginAudit, LoginError
from login.service import KBSoftwareOIDCAuthenticationBackend
from .factories import UserFactory


# PJK 27/04/2020, Too complicated for now.  Will take a bit of time (I think)
# def _token():
#     """
#
#     Copied from:
#     https://github.com/mozilla/mozilla-django-oidc/blob/master/tests/test_auth.py
#
#     """
#     header = force_bytes(json.dumps({"alg": "none"}))
#     payload = force_bytes(json.dumps({"foo": "bar"}))
#     signature = ""
#     # return force_bytes(
#     return "{}.{}.{}".format(
#         smart_text(b64encode(header)),
#         smart_text(b64encode(payload)),
#         signature,
#     )
#
#
# @responses.activate
# def test_authenticate_ember():
#     x = KBSoftwareOIDCAuthenticationBackend()
#     responses.add(
#         responses.GET,
#         "https://login.microsoftonline.com/common/discovery/v2.0/keys",
#         json={"a": 1},
#         status=HTTPStatus.OK,
#     )
#     responses.add(
#         responses.POST,
#         settings.OIDC_OP_TOKEN_ENDPOINT,
#         json={"access_token": "my-access-token", "id_token": _token()},
#         status=HTTPStatus.OK,
#     )
#     assert x.authenticate_ember("a1b2c3d4", "my-state") is None


@pytest.mark.django_db
def test_authenticate_ember_testing_with_cypress(settings):
    settings.DEBUG = True
    settings.KB_TEST_EMAIL_USERNAME = "pkimber"
    settings.USE_OPENID_CONNECT = False
    user = UserFactory(username="pkimber")
    x = KBSoftwareOIDCAuthenticationBackend()
    assert user == x.authenticate_ember("kb-testing-with-cypress")


@pytest.mark.django_db
def test_authenticate_ember_testing_with_cypress_another_user(settings):
    settings.DEBUG = True
    settings.KB_TEST_EMAIL_USERNAME = "pkimber"
    settings.USE_OPENID_CONNECT = False
    user = UserFactory(username="different-user-name")
    x = KBSoftwareOIDCAuthenticationBackend()
    user_model = get_user_model()
    with pytest.raises(user_model.DoesNotExist):
        x.authenticate_ember("kb-testing-with-cypress")


@pytest.mark.django_db
def test_authenticate_ember_testing_with_cypress_use_openid_connect(settings):
    settings.DEBUG = True
    settings.KB_TEST_EMAIL_USERNAME = "pkimber"
    settings.USE_OPENID_CONNECT = False
    user = UserFactory(username="pkimber")
    x = KBSoftwareOIDCAuthenticationBackend()
    with pytest.raises(LoginError) as e:
        x.authenticate_ember("kb")
    assert (
        "'USE_OPENID_CONNECT' must be 'True' when using "
        "'KBSoftwareOIDCAuthenticationBackend' "
        "(unless testing with 'kb-testing-with-cypress')"
    ) in str(e.value)


@pytest.mark.django_db
def test_filter_users_by_claims():
    """The email address does exist."""
    user = UserFactory(email="web@pkimber.net")
    x = KBSoftwareOIDCAuthenticationBackend()
    assert 0 == LoginAudit.objects.count()
    qs = x.filter_users_by_claims({"email": "web@pkimber.net"})
    assert [user] == [user for user in qs]
    assert 0 == LoginAudit.objects.count()


@pytest.mark.django_db
def test_filter_users_by_claims_email_does_not_exist():
    """The email address does not exist."""
    UserFactory(email="patrick@pkimber.net")
    x = KBSoftwareOIDCAuthenticationBackend()
    assert 0 == LoginAudit.objects.count()
    with freeze_time(datetime(2017, 5, 21, 19, 35)):
        qs = x.filter_users_by_claims({"email": "web@pkimber.net"})
    assert 0 == qs.count()
    assert 1 == LoginAudit.objects.count()
    login_audit = LoginAudit.objects.first()
    assert "{}. 21/05/2017 19:35 (email-does-not-exist) web@pkimber.net".format(
        login_audit.pk
    ) == str(login_audit)


@pytest.mark.django_db
def test_filter_users_by_claims_email_is_duplicate():
    """The email address is duplicate.

    Active Directory Sync - deleted accounts with a valid email address can
    prevent an active account from logging on.

    Check the audit tells us when we have a duplicate email address.

    https://www.kbsoftware.co.uk/crm/ticket/7271/

    """
    UserFactory(username="user-1", email="web@pkimber.net", is_active=True)
    UserFactory(username="user-2", email="web@pkimber.net", is_active=True)
    x = KBSoftwareOIDCAuthenticationBackend()
    assert 0 == LoginAudit.objects.count()
    with freeze_time(datetime(2017, 5, 21, 19, 35)):
        qs = x.filter_users_by_claims({"email": "web@pkimber.net"}).order_by(
            "username"
        )
    assert ["user-1", "user-2"] == [user.username for user in qs]
    assert 1 == LoginAudit.objects.count()
    login_audit = LoginAudit.objects.first()
    assert (
        f"{login_audit.pk}. 21/05/2017 19:35 (duplicate-email) web@pkimber.net"
        == str(login_audit)
    )


@pytest.mark.django_db
def test_filter_users_by_claims_filter_is_active():
    """The email address is duplicate.

    Active Directory Sync - deleted accounts with a valid email address can
    prevent an active account from logging on.

    https://www.kbsoftware.co.uk/crm/ticket/7271/

    """
    UserFactory(username="user-1", email="web@pkimber.net", is_active=False)
    UserFactory(username="user-2", email="web@pkimber.net", is_active=True)
    x = KBSoftwareOIDCAuthenticationBackend()
    assert 0 == LoginAudit.objects.count()
    qs = x.filter_users_by_claims({"email": "web@pkimber.net"})
    assert ["user-2"] == [user.username for user in qs]
    assert 0 == LoginAudit.objects.count()


@pytest.mark.django_db
def test_get_userinfo():
    x = KBSoftwareOIDCAuthenticationBackend()
    assert {"email": "web@pkimber.net"} == x.get_userinfo(
        None, None, {"email": "web@pkimber.net"}
    )


@pytest.mark.django_db
def test_get_userinfo_no_email(client):
    x = KBSoftwareOIDCAuthenticationBackend()
    assert 0 == LoginAudit.objects.count()
    with freeze_time(datetime(2017, 5, 21, 19, 35)):
        assert {"email": None} == x.get_userinfo(None, None, {"fruit": "Apple"})
    assert 1 == LoginAudit.objects.count()
    login_audit = LoginAudit.objects.first()
    assert "{}. 21/05/2017 19:35 (no-email-in-payload) ['fruit']".format(
        login_audit.pk
    ) == str(login_audit)


@pytest.mark.parametrize(
    "host_name",
    ["https://www.kbsoftware.co.uk", "https://www.kbsoftware.co.uk/"],
)
def test_token_payload(host_name, settings):
    settings.HOST_NAME = host_name
    x = KBSoftwareOIDCAuthenticationBackend()
    assert {
        "client_id": settings.OIDC_RP_CLIENT_ID,
        "client_secret": settings.OIDC_RP_CLIENT_SECRET,
        "grant_type": "authorization_code",
        "code": "a1b2c3d4",
        "redirect_uri": "http://localhost:4200/login",
    } == x._token_payload("a1b2c3d4")
