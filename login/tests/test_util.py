# -*- encoding: utf-8 -*-
import pytest

from django.contrib.auth import get_user_model

from login.models import SYSTEM_GENERATED
from login.util import get_system_generated_user


@pytest.mark.django_db
def test_get_system_generated_user():
    sys_user = get_system_generated_user()
    assert sys_user.username == SYSTEM_GENERATED

    user = get_user_model().objects.get(username=SYSTEM_GENERATED)
    assert user.username == SYSTEM_GENERATED
    sys_user = get_system_generated_user()
    assert sys_user.username == SYSTEM_GENERATED
