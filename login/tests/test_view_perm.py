# -*- encoding: utf-8 -*-
import pytest

from django.urls import reverse

from login.tests.fixture import perm_check


@pytest.mark.django_db
def test_login_audit_list(perm_check):
    url = reverse("login.audit.list")
    perm_check.superuser(url)


@pytest.mark.django_db
def test_password_reset_audit_list_view(perm_check):
    perm_check.staff(reverse("password_reset_audit_report"))
