# -*- encoding: utf-8 -*-
import pytest

from django.contrib.auth.models import User

from .factories import UserFactory
from .model_maker import make_user, make_superuser


@pytest.mark.django_db
def test_make_user():
    assert 0 == User.objects.count()
    user = make_user("patrick")
    x = User.objects.get(username="patrick")
    assert x == user


@pytest.mark.django_db
def test_make_user_existing():
    UserFactory(username="patrick")
    assert 1 == User.objects.count()
    user = make_user("patrick")
    x = User.objects.get(username="patrick")
    assert x == user


@pytest.mark.django_db
def test_make_superuser():
    assert 0 == User.objects.count()
    user = make_superuser("patrick")
    x = User.objects.get(username="patrick")
    assert x == user
    assert x.is_staff is True
    assert x.is_superuser is True


@pytest.mark.django_db
def test_make_superuser_existing():
    UserFactory(username="patrick")
    assert 1 == User.objects.count()
    user = make_superuser("patrick")
    x = User.objects.get(username="patrick")
    assert x == user
    assert x.is_staff is True
    assert x.is_superuser is True
