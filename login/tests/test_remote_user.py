# -*- encoding: utf-8 -*-
"""
Trying to use::

  AUTHENTICATION_BACKENDS = ("login.auth.ExistingRemoteUserBackend",)

and::

 MIDDLEWARE = (
     "django.contrib.auth.middleware.AuthenticationMiddleware",
     "django.contrib.auth.middleware.PersistentRemoteUserMiddleware",

So using these tests to first of all learn about it, then to create our own
class (if required).

Tests copied from:
https://github.com/django/django/blob/master/tests/auth_tests/test_remote_user.py

"""
import pytest

from django.contrib.auth import get_user_model

from login.auth import ExistingRemoteUserBackend


@pytest.mark.django_db
def test_clean_username():
    backend = ExistingRemoteUserBackend()
    assert "patrick@www.kbsoftware.co.uk" == backend.clean_username(
        "www.kbsoftware.co.uk\\patrick"
    )


@pytest.mark.django_db
def test_clean_username_mixed_case():
    backend = ExistingRemoteUserBackend()
    assert "Patrick@KBSoftware" == backend.clean_username("KBSoftware\\Patrick")


@pytest.mark.django_db
def test_clean_username_simple():
    backend = ExistingRemoteUserBackend()
    assert "patrick@kbsoftware" == backend.clean_username("kbsoftware\\patrick")


@pytest.mark.django_db
def test_clean_username_no_domain():
    backend = ExistingRemoteUserBackend()
    assert "patrick" == backend.clean_username("patrick")


@pytest.mark.django_db
@pytest.mark.urls("login.tests.urls_remote_user")
def test_domain_user(client):
    """Tests the case where the username includes a domain name."""
    get_user_model().objects.create(username="patrick.kimber@KB")
    num_users = get_user_model().objects.count()
    response = client.get(
        "/remote_user/", **{"REMOTE_USER": "kb\\Patrick.Kimber"}
    )
    assert "patrick.kimber@KB" == response.context["user"].username
    assert num_users == get_user_model().objects.count()


@pytest.mark.django_db
@pytest.mark.urls("login.tests.urls_remote_user")
def test_known_user(client):
    """Tests the case where the username passed in the header is a valid User.

    Copied from:
    https://github.com/django/django/blob/master/tests/auth_tests/test_remote_user.py

    """
    get_user_model().objects.create(username="knownuser")
    get_user_model().objects.create(username="knownuser2")
    num_users = get_user_model().objects.count()
    response = client.get("/remote_user/", **{"REMOTE_USER": "knownuser"})
    assert "knownuser" == response.context["user"].username
    assert num_users == get_user_model().objects.count()
    # A different user passed in the headers causes the new user to be logged in
    response = client.get("/remote_user/", **{"REMOTE_USER": "knownuser2"})
    assert "knownuser2" == response.context["user"].username
    assert num_users == get_user_model().objects.count()


@pytest.mark.django_db
@pytest.mark.urls("login.tests.urls_remote_user")
def test_unknown_user(client):
    """We don't want unknown users to be created, so copy this test.

    Test copied from:
    https://github.com/django/django/blob/master/tests/auth_tests/test_remote_user.py

    Contains the same tests as RemoteUserTest, but using a custom auth backend
    class that doesn't create unknown users.

    """
    num_users = get_user_model().objects.count()
    response = client.get("/remote_user/", **{"REMOTE_USER": "newuser"})
    assert response.context["user"].is_anonymous is True
    assert num_users == get_user_model().objects.count()
