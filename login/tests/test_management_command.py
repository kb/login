# -*- encoding: utf-8 -*-
import pytest

from django.core.management import call_command

from .factories import UserFactory


@pytest.mark.django_db
def test_create_user_system_generated():
    call_command("create-user-system-generated")


@pytest.mark.django_db
def test_demo_data_login():
    call_command("demo-data-login")


@pytest.mark.django_db
def test_list_of_users():
    UserFactory()
    call_command("list-of-users", "temp-list-of-users.csv")
