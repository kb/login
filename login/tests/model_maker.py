# -*- encoding: utf-8 -*-
from django.contrib.auth.models import User
from django.utils import timezone

from .factories import TEST_PASSWORD


def make_user(user_name, **kwargs):
    """Some code copied from 'contrib.auth.models.create_user'"""
    try:
        user = User.objects.get(username=user_name)
    except User.DoesNotExist:
        defaults = {
            "date_joined": timezone.now(),
            "is_active": True,
            "is_staff": False,
            "is_superuser": False,
        }
        defaults.update(kwargs)
        user = User(username=user_name, **defaults)
        user.set_password(TEST_PASSWORD)
        user.save()
    return user


def make_superuser(user_name):
    try:
        update = False
        user = User.objects.get(username=user_name)
        if not user.is_staff:
            user.is_staff = True
            update = True
        if not user.is_superuser:
            user.is_superuser = True
            update = True
        if update:
            user.save()
    except User.DoesNotExist:
        user = User.objects.create_superuser(user_name, "", TEST_PASSWORD)
    return user
