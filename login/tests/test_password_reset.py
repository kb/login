# -*- encoding: utf-8 -*-
import pytest

from django.core import mail
from django.urls import reverse
from http import HTTPStatus

from login.tests.factories import UserFactory
from mail.models import Mail
from mail.tests.factories import NotifyFactory


@pytest.mark.django_db
@pytest.mark.parametrize(
    "recaptcha_private_key,form_fields_expect",
    [
        (None, ["email"]),
        ("abcdef", ["email", "captcha"]),
    ],
)
def test_password_reset_get(
    client, settings, recaptcha_private_key, form_fields_expect
):
    """Test with and without Google ReCaptcha."""
    settings.RECAPTCHA_PRIVATE_KEY = recaptcha_private_key
    url = reverse("password_reset")
    response = client.get(url)
    assert HTTPStatus.OK == response.status_code
    form = response.context["form"]
    assert form_fields_expect == list(form.fields.keys())


@pytest.mark.django_db
def test_password_reset_complete(client):
    response = client.get(reverse("password_reset_complete"))
    assert HTTPStatus.OK == response.status_code


@pytest.mark.django_db
def test_password_reset_confirm(client):
    url = reverse(
        "password_reset_confirm", args=["MjMw", "4gm-ce2d613ad336af07d1c9"]
    )
    response = client.get(url)
    assert HTTPStatus.OK == response.status_code


@pytest.mark.django_db
def test_password_reset_confirm_post(client, mailoutbox):
    """Try and test the password reset confirm view.

    .. note:: I can't work out how to test the actual password reset, so will
              leave it for now.

    """
    # start by getting the password reset URL
    url = None
    user = UserFactory(username="patrick", email="web@pkimber.net")
    response = client.post(
        reverse("password_reset"),
        {
            "email": user.email,
            "g-recaptcha-response": "PASSED",
        },
    )
    assert HTTPStatus.FOUND == response.status_code
    assert reverse("password_reset_done") == response.url
    assert len(mailoutbox) == 1
    m = mailoutbox[0]
    for line in m.body.split("\n"):
        if line.startswith("http://testserver/"):
            url = line
    assert url is not None, "Cannot find password reset URL: {}".format(m.body)
    # now do the actual test!
    response = client.post(url)
    assert HTTPStatus.FOUND == response.status_code
    # assert reverse('password_reset_complete') == response.url


@pytest.mark.django_db
def test_password_reset_done(client):
    response = client.get(reverse("password_reset_done"))
    assert HTTPStatus.OK == response.status_code


@pytest.mark.django_db
def test_password_reset_email(client, mailoutbox):
    """Send a notification email and a reset email."""
    NotifyFactory()
    UserFactory(username="web", email="test@pkimber.net")
    response = client.post(
        reverse("password_reset"),
        {
            "email": "test@pkimber.net",
            "g-recaptcha-response": "PASSED",
        },
    )
    assert HTTPStatus.FOUND == response.status_code
    assert reverse("password_reset_done") in response["Location"]
    result = [outbox.subject for outbox in mail.outbox]
    assert 1 == len(mailoutbox)
    assert "Password reset on testserver" in result
    assert 1 == Mail.objects.count()
    obj = Mail.objects.first()
    assert obj.message.subject == "Password reset request from test@pkimber.net"


@pytest.mark.django_db
def test_password_reset_email_does_not_exist(client, mailoutbox):
    """Only send a notification if email address doesn't exist."""
    NotifyFactory()
    response = client.post(
        reverse("password_reset"),
        {
            "email": "test@pkimber.net",
            "g-recaptcha-response": "PASSED",
        },
    )
    assert HTTPStatus.FOUND == response.status_code
    assert reverse("password_reset_done") in response["Location"]
    # Check that only the request notification email has been sent.
    assert 0 == len(mailoutbox)
    assert 1 == Mail.objects.count()
    obj = Mail.objects.first()
    assert obj.message.subject == "Password reset request from test@pkimber.net"


@pytest.mark.django_db
def test_password_reset_post(client, mailoutbox):
    """Check the password reset view.

    .. note:: This test was copied from another project, so is possible a
              duplicate of one of the tests above.

    """
    NotifyFactory(email="test@pkimber.net")
    user = UserFactory(email="web@pkimber.net")
    url = reverse("password_reset")
    response = client.post(
        url,
        {
            "email": user.email,
            "g-recaptcha-response": "PASSED",
        },
    )
    assert HTTPStatus.FOUND == response.status_code
    assert reverse("password_reset_done") == response.url
    mail = Mail.objects.get(email="test@pkimber.net")
    assert (
        "Password reset request from web@pkimber.net"
    ) == mail.message.subject
    assert (
        "Password reset email sent.  "
        "(user is active and has a usable password)"
    ) in mail.message.description
    assert len(mailoutbox) == 1
    m = mailoutbox[0]
    assert "Password reset on testserver" == m.subject
    assert (
        "You're receiving this email because you requested "
        "a password reset for your user account at testserver"
    ) in m.body
    assert "notify@pkimber.net" == m.from_email
    assert list(m.to) == ["web@pkimber.net"]


@pytest.mark.django_db
def test_password_reset_post_no_captcha(client, settings):
    """Check the password reset view with no captcha entered."""
    settings.RECAPTCHA_PRIVATE_KEY = "abc"
    NotifyFactory(email="test@pkimber.net")
    user = UserFactory(email="web@pkimber.net")
    url = reverse("password_reset")
    response = client.post(url, {"email": user.email})
    assert HTTPStatus.OK == response.status_code
    assert {"captcha": ["This field is required."]} == response.context[
        "form"
    ].errors
