# -*- encoding: utf-8 -*-
import pytest

"""
Test login and logout templates

Django Test Client: Test that user was logged in:
http://stackoverflow.com/questions/5660952/django-test-client-test-that-user-was-logged-in

"""

from django.contrib.auth import SESSION_KEY
from django.urls import reverse
from http import HTTPStatus

from login.tests.factories import TEST_PASSWORD
from login.tests.model_maker import make_user


@pytest.mark.django_db
def test_login(client):
    user_pk = make_user("patrick").pk
    # check the user is not logged in
    assert SESSION_KEY not in client.session
    # login the user
    data = {"username": "patrick", "password": TEST_PASSWORD}
    url = reverse("login")
    response = client.post(url, data)
    # check the user was redirected
    assert HTTPStatus.FOUND == response.status_code
    # check the user was logged in
    assert SESSION_KEY in client.session
    assert user_pk == int(client.session[SESSION_KEY])


@pytest.mark.django_db
def test_login_next(client):
    """Check that login with a 'next' parameter is working correctly"""
    user_pk = make_user("patrick").pk
    # check the user is not logged in
    assert SESSION_KEY not in client.session
    # login the user
    data = {"username": "patrick", "password": TEST_PASSWORD}
    url = "%s?next=/test/" % reverse("login")
    response = client.post(url, data)
    # check the user was redirected to the test page
    assert HTTPStatus.FOUND == response.status_code
    assert reverse("example.test") == response.url
    # check the user was logged in
    assert SESSION_KEY in client.session
    assert user_pk == int(client.session[SESSION_KEY])


@pytest.mark.django_db
def test_logout(client):
    user_pk = make_user("patrick").pk
    # check the user is not logged in
    assert SESSION_KEY not in client.session
    # login the user
    client.login(username="patrick", password=TEST_PASSWORD)
    # check the user was logged in
    assert SESSION_KEY in client.session
    assert user_pk == int(client.session[SESSION_KEY])
    # logout the user
    url = reverse("logout")
    response = client.post(url)
    # check the user was redirected to 'settings.LOGOUT_REDIRECT_URL'
    assert HTTPStatus.FOUND == response.status_code
    assert reverse("project.home") == response.url
    # check the user was logged out
    assert SESSION_KEY not in client.session
