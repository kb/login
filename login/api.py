# -*- encoding: utf-8 -*-
import logging

from django.core.exceptions import ObjectDoesNotExist
from rest_framework.authtoken.models import Token
from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.exceptions import AuthenticationFailed
from rest_framework.response import Response

from .serializers import AuthTokenOnCodeSerializer


logger = logging.getLogger(__name__)


class ObtainAuthTokenOnCode(ObtainAuthToken):
    """

    Uses:

    - ``AuthTokenOnCodeSerializer`` (``login/serializers.py``)
    - ``KBSoftwareOIDCAuthenticationBackend`` (``login.service.py)``

    """

    serializer_class = AuthTokenOnCodeSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        try:
            serializer.is_valid(raise_exception=True)
        except Exception as ex:
            logger.exception("Login error")
            raise AuthenticationFailed(
                f"The server reported the following error: {ex}."
            )
        user = serializer.validated_data["user"]
        token, created = Token.objects.get_or_create(user=user)
        try:
            contact = user.contact
        except ObjectDoesNotExist:
            message = "User '{}' does not have a 'Contact'".format(
                user.username
            )
            logger.error(message)
            raise AuthenticationFailed(message)
        return Response({"token": token.key, "contact_id": contact.pk})
