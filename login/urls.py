# -*- encoding: utf-8 -*-
from django.conf import settings
from django.contrib.auth.views import (
    LoginView,
    # LogoutView,
    PasswordChangeDoneView,
    PasswordChangeView,
    PasswordResetCompleteView,
    PasswordResetConfirmView,
    PasswordResetDoneView,
    PasswordResetView,
)
from django.urls import path, re_path, reverse_lazy
from django.utils import timezone

from .forms import PasswordResetNotifyForm
from .views import (
    is_auth,
    logout_view,
    LoginAuditListView,
    PasswordResetAuditListView,
)


def timezone_now():
    return timezone.now()


urlpatterns = [
    re_path(r"^accounts/is/auth/$", view=is_auth, name="login.is.auth"),
    re_path(
        r"^accounts/login/$",
        view=LoginView.as_view(
            extra_context={
                "path": reverse_lazy("login"),  # "/accounts/login/",
                "testing": settings.TESTING,
                "today": timezone_now,
                "use_openid_connect": settings.USE_OPENID_CONNECT,
            },
            template_name="login/login.html",
        ),
        name="login",
    ),
    re_path(
        r"^accounts/logout/$",
        view=logout_view,
        name="logout",
    ),
    re_path(
        r"^accounts/password/change/$",
        view=PasswordChangeView.as_view(
            extra_context={
                "testing": settings.TESTING,
                "today": timezone_now,
            },
            template_name="login/password_change.html",
        ),
        name="password_change",
    ),
    re_path(
        r"^accounts/password/change/done/$",
        view=PasswordChangeDoneView.as_view(
            extra_context={
                "testing": settings.TESTING,
                "today": timezone_now,
            },
            template_name="login/password_change_done.html",
        ),
        name="password_change_done",
    ),
    re_path(
        r"^accounts/password/reset/$",
        view=PasswordResetView.as_view(
            extra_context={
                "testing": settings.TESTING,
                "today": timezone_now,
            },
            form_class=PasswordResetNotifyForm,
            template_name="login/password_reset.html",
        ),
        kwargs={"extra_context": {"testing": settings.TESTING}},
        name="password_reset",
    ),
    # https://westcountrycoders.co.uk/accounts/password/reset/NTMxNg/3sc-d2a52e19e0d239ea538e/
    # Reverse for 'password_reset_complete' with arguments '()' and keyword arguments '{}' not found. 0 pattern(s) tried: []
    # password_reset_complete
    re_path(
        r"^accounts/password/reset/complete/$",
        view=PasswordResetCompleteView.as_view(
            extra_context={
                "testing": settings.TESTING,
                "today": timezone_now,
            },
            template_name="login/password_reset_complete.html",
        ),
        name="password_reset_complete",
    ),
    path(
        r"accounts/password/reset/<uidb64>/<token>/confirm/",
        view=PasswordResetConfirmView.as_view(
            extra_context={
                "testing": settings.TESTING,
                "today": timezone_now,
            },
            template_name="login/password_reset_confirm.html",
        ),
        name="password_reset_confirm",
    ),
    re_path(
        r"^accounts/password/reset/done/$",
        view=PasswordResetDoneView.as_view(
            extra_context={
                "testing": settings.TESTING,
                "today": timezone_now,
            },
            template_name="login/password_reset_done.html",
        ),
        name="password_reset_done",
    ),
    re_path(
        r"^accounts/password/reset/audit/report/$",
        view=PasswordResetAuditListView.as_view(),
        name="password_reset_audit_report",
    ),
    re_path(
        r"^login/audit/$",
        view=LoginAuditListView.as_view(),
        name="login.audit.list",
    ),
]
