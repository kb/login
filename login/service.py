# -*- encoding: utf-8 -*-
import logging
import urllib.parse

from django.conf import settings
from django.contrib.auth import get_user_model
from django.core.exceptions import SuspiciousOperation
from mozilla_django_oidc.auth import OIDCAuthenticationBackend

from .models import LoginAudit, LoginError


logger = logging.getLogger(__name__)


class FakeRequest:
    def __init__(self):
        self.session = {}


class KBSoftwareOIDCAuthenticationBackend(OIDCAuthenticationBackend):
    """

    Uses:

    - ``AuthTokenOnCodeSerializer`` (``login/serializers.py``)
    - ``ObtainAuthTokenOnCode`` (``login.api.py)``

    """

    def _token_payload(self, code):
        return {
            "client_id": self.OIDC_RP_CLIENT_ID,
            "client_secret": self.OIDC_RP_CLIENT_SECRET,
            "grant_type": "authorization_code",
            "code": code,
            "redirect_uri": urllib.parse.urljoin(
                settings.OPEN_ID_CONNECT_EMBER_REDIRECT_URI,
                settings.OPEN_ID_CONNECT_EMBER_REDIRECT_ROUTE,
            ),
        }

    def authenticate_ember(self, code):
        """

        Copied from ``authenticate`` (``mozilla_django_oidc/auth.py``)

        Used by Django projects as an ``AUTHENTICATION_BACKENDS`` and by Ember
        apps in ``AuthTokenOnCodeSerializer``.

        """
        if settings.USE_OPENID_CONNECT:
            token_payload = self._token_payload(code)
            # Get the token
            token_info = self.get_token(token_payload)
            id_token = token_info.get("id_token")
            access_token = token_info.get("access_token")
            # Validate the token
            payload = self.verify_token(id_token, nonce=settings.OIDC_USE_NONCE)
            if payload:
                self.request = FakeRequest()
                self.store_tokens(access_token, id_token)
                try:
                    return self.get_or_create_user(
                        access_token, id_token, payload
                    )
                except SuspiciousOperation as exc:
                    logger.warning("failed to get or create user: %s", exc)
                    return None
        elif settings.DEBUG and code == "kb-testing-with-cypress":
            return get_user_model().objects.get(
                username=settings.KB_TEST_EMAIL_USERNAME
            )
        else:
            raise LoginError(
                "'USE_OPENID_CONNECT' must be 'True' when "
                "using 'KBSoftwareOIDCAuthenticationBackend' "
                "(unless testing with 'kb-testing-with-cypress')"
            )
        return None

    def filter_users_by_claims(self, claims):
        """Return all users matching the specified email.

        .. note:: Copied from ``mozilla_django_oidc/auth.py``

        """
        email = claims.get("email")
        if not email:
            return self.UserModel.objects.none()
        users = self.UserModel.objects.filter(is_active=True).filter(
            email__iexact=email
        )
        if len(users) == 0:
            LoginAudit.objects.create_login_audit(
                LoginAudit.EMAIL_DOES_NOT_EXIST, email=email
            )
        elif len(users) > 1:
            LoginAudit.objects.create_login_audit(
                LoginAudit.DUPLICATE_EMAIL, email=email
            )
        return users

    def get_userinfo(self, access_token, id_token, payload):
        email = payload.get("email")
        if not email:
            LoginAudit.objects.create_login_audit(
                LoginAudit.MISSING_EMAIL, payload=payload
            )
        return {"email": email}
