# -*- encoding: utf-8 -*-
from django.core.management.base import BaseCommand

from login.util import get_system_generated_user


class Command(BaseCommand):
    help = "Create the 'system generated' user"

    def handle(self, *args, **options):
        self.stdout.write(f"{self.help}")
        system_generated_user = get_system_generated_user()
        self.stdout.write(
            f"{self.help} - created '{system_generated_user.username}' - complete"
        )
