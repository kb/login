"""Usage::

    # Use the designated user from settings: KB_TEST_EMAIL_USERNAME & KB_TEST_EMAIL_FOR_OIDC.
    django-admin demo-data-login-oidc

    # Specify the user.
    django-admin demo-data-login-oidc daniel.driver tim.bushell@kbuk.onmicrosoft.com

"""  # -*- encoding: utf-8 -*-

from django.conf import settings
from django.contrib.auth import get_user_model
from django.core.management.base import BaseCommand

from login.models import LoginError


class Command(BaseCommand):
    help = "Create demo data for 'login' with OIDC"

    def add_arguments(self, parser):
        parser.add_argument(
            "user_name",
            type=str,
            nargs="?",
            default=settings.KB_TEST_EMAIL_USERNAME,
            help="The user name of the user.",
        )
        parser.add_argument(
            "email",
            type=str,
            nargs="?",
            default=settings.KB_TEST_EMAIL_FOR_OIDC,
            help="An email address which would be recognised by your OIDC host",
        )

    def handle(self, *args, **options):
        user_model = get_user_model()
        user_name = options["user_name"]
        self.stdout.write("user_name: {}".format(user_name))
        email = options["email"]
        # Find and amend the designated user.
        found = False
        try:
            user = user_model.objects.get(username=user_name)
            found = True
        except user_model.DoesNotExist:
            count = 0
            user_names = []
            for x in user_model.objects.all():
                user_names.append(x.username)
                count = count + 1
                if count > 5:
                    break
            raise LoginError(
                "Cannot find user '{}' (you could try {})".format(
                    user_name, user_names
                )
            )
        if found:
            user.email = email
            user.save()
            self.stdout.write(
                "email address for '{}': {}".format(user.username, user.email)
            )
        self.stdout.write("{} - Complete".format(self.help))
