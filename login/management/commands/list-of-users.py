# -*- encoding: utf-8 -*-
import csv

from django.contrib.auth import get_user_model
from django.core.management.base import BaseCommand


class Command(BaseCommand):
    help = "List of users (in CSV format)"

    def add_arguments(self, parser):
        parser.add_argument("file_name", nargs="+")

    def handle(self, *args, **options):
        self.stdout.write(f"{self.help}")
        for file_name in options["file_name"]:
            with open(file_name, "w", newline="") as out:
                csv_writer = csv.writer(out, dialect="excel-tab")
                qs = (
                    get_user_model()
                    .objects.filter(is_active=True)
                    .order_by("username")
                )
                self.stdout.write(f"Found {qs.count()} users")
                for user in qs:
                    csv_writer.writerow([user.username, user.get_full_name()])
                    self.stdout.write(user.username)
        self.stdout.write(f"{self.help} - Complete")
