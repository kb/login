# -*- encoding: utf-8 -*-

from django import template
from django.contrib.auth.models import Group


register = template.Library()


def has_role(user, role_name):
    if user.is_superuser:
        return True
    try:
        user.groups.get(name=role_name)
        return True
    except Group.DoesNotExist:
        pass
    return False


register.filter(has_role)
