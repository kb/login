# -*- encoding: utf-8 -*-
from braces.views import (
    AnonymousRequiredMixin,
    LoginRequiredMixin,
    StaffuserRequiredMixin,
    SuperuserRequiredMixin,
)
from django.conf import settings
from django.contrib import messages
from django.contrib.auth import authenticate, login
from django.contrib.auth import logout
from django.contrib.auth.forms import SetPasswordForm, UserCreationForm
from django.contrib.auth.models import User
from django.contrib.auth.views import redirect_to_login
from django.core.exceptions import PermissionDenied
from django.http import HttpResponse
from django.shortcuts import redirect
from django.urls import reverse
from django.views.generic import CreateView, ListView, UpdateView

from http import HTTPStatus

from base.view_utils import BaseMixin
from .forms import UserNameForm
from .models import LoginAudit, PasswordResetAudit


def is_auth(request):
    """All proxied requests to be authorized by Django.

    Django application as an authentication / authorization server for Shiny:
    http://pawamoy.github.io/2018/03/15/django-auth-server-for-shiny/

    """
    if request.user.is_authenticated:
        return HttpResponse(status=HTTPStatus.OK)
    return HttpResponse(status=HTTPStatus.FORBIDDEN)


def logout_view(request):
    logout(request)
    messages.add_message(request, messages.INFO, "You have been logged out...")
    return redirect(reverse("project.home"))


class RoleRequiredMixin:
    """Determine if access is allowed based on a role

    Typically to use, subclass this mixin and override the get_role method to
    return a django group reresenting the role.

    Other configuration parameters that can be specified are::

    DEFAULT_ALLOW: If the role is not defined is access allowed.  This allows
    backwards compatibility if the role is added during the product lifecycle

    STAFF_REQUIRED: Is it required that the user have the is_staff flag set

    Typical Use::

      This mixin requires a subclass to be created which defines the role e.g.

          class ARoleRequiredMixin(RoleRequiredMixin):

              def get_role(self):
                  return Group.objects.get(name="A")

      This mixin can then be used as follows::

          class RoleView(AccessMixin, ARoleRequiredMixin, BaseMixin, View):

    """

    STAFF_REQUIRED = True
    DEFAULT_ALLOW = True

    def get_role(self):
        return None

    def dispatch(self, request, *args, **kwargs):
        user = request.user
        role = self.get_role()

        # test
        allow = user.is_authenticated and (
            user.is_staff or not self.STAFF_REQUIRED
        )

        if allow and not user.is_superuser:
            if role:
                allow = role in user.groups.all()
            else:
                allow = self.DEFAULT_ALLOW

        # action
        if not allow:
            if user.is_authenticated:
                raise PermissionDenied  # return a forbidden response
            else:
                return redirect_to_login(
                    request.get_full_path(),
                    self.get_login_url(),
                    self.get_redirect_field_name(),
                )
        return super().dispatch(request, *args, **kwargs)


class LoginAuditListView(
    LoginRequiredMixin, SuperuserRequiredMixin, BaseMixin, ListView
):
    model = LoginAudit
    paginate_by = 20


class PasswordResetAuditListView(
    LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, ListView
):
    model = PasswordResetAudit
    paginate_by = 20


class RegisterCreateView(AnonymousRequiredMixin, BaseMixin, CreateView):
    form_class = UserCreationForm
    model = User
    template_name = "login/register.html"

    def form_valid(self, form):
        username = form.cleaned_data["username"]
        password = form.clean_password2()
        result = super().form_valid(form)
        # log in the user
        user = authenticate(username=username, password=password)
        if user:
            login(self.request, user)
        return result

    def get_success_url(self):
        return settings.LOGIN_REDIRECT_URL


class UpdateUserNameMixin:
    """Mixin so that other projects can set-up their own permissions."""

    form_class = UserNameForm
    model = User
    template_name = "login/update_user_name.html"


class UpdateUserNameView(
    LoginRequiredMixin,
    StaffuserRequiredMixin,
    UpdateUserNameMixin,
    BaseMixin,
    UpdateView,
):
    """A member of staff can change the user name of a user."""

    pass


class UpdateUserPasswordMixin:
    """Mixin so that other projects can set-up their own permissions."""

    form_class = SetPasswordForm
    model = User
    template_name = "login/update_user_password.html"

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs.update(dict(user=self.object))
        # the form throws an error unless I remove this element.
        kwargs.pop("instance")
        return kwargs


class UpdateUserPasswordView(
    LoginRequiredMixin,
    StaffuserRequiredMixin,
    UpdateUserPasswordMixin,
    BaseMixin,
    UpdateView,
):
    """A member of staff can change the password of a user."""

    pass
