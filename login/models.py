# -*- encoding: utf-8 -*-
import re

from datetime import date

from django.core.serializers.json import DjangoJSONEncoder
from django.core.validators import RegexValidator
from django.db import models

from base.model_utils import TimeStampedModel


SYSTEM_GENERATED = "system.generated"
username_validator = RegexValidator(
    re.compile("^[\w.@+-]+$"), "Enter a valid username.", "invalid"
)


class LoginError(Exception):
    def __init__(self, value):
        Exception.__init__(self)
        self.value = value

    def __str__(self):
        return repr("{}, {}".format(self.__class__.__name__, self.value))


class LoginAuditManager(models.Manager):
    def create_login_audit(self, slug, email=None, payload=None):
        x = self.model(slug=slug, email=email or "", payload=payload)
        x.save()
        return x


class LoginAudit(TimeStampedModel):
    DUPLICATE_EMAIL = "duplicate-email"
    EMAIL_DOES_NOT_EXIST = "email-does-not-exist"
    MISSING_EMAIL = "no-email-in-payload"

    slug = models.SlugField()
    email = models.EmailField(blank=True)
    payload = models.JSONField(blank=True, null=True, encoder=DjangoJSONEncoder)
    objects = LoginAuditManager()

    class Meta:
        ordering = ["-modified"]
        verbose_name = "Login audit"
        verbose_name_plural = "Login audit"

    def __str__(self):
        result = None
        created = self.created.strftime("%d/%m/%Y %H:%M")
        if self.email:
            result = "{}. {} ({}) {}".format(
                self.pk, created, self.slug, self.email
            )
        else:
            result = "{}. {} ({}) {}".format(
                self.pk, created, self.slug, list(self.payload.keys())
            )
        return result


class PasswordResetAuditManager(models.Manager):
    def audit(self, email):
        try:
            obj = self.model.objects.get(email__iexact=email)
            obj.count = obj.count + 1
            obj.save()
        except PasswordResetAudit.DoesNotExist:
            obj = self.model(email=email, request_date=date.today(), count=1)
            obj.save()
        return obj


class PasswordResetAudit(TimeStampedModel):
    """Record the password reset requests.

    Use a 'count' rather than a new record every time, in case we get a DOS
    attack.  Password reset requests are not protected by a login, so we could
    run out of disk space on our database server.

    """

    email = models.EmailField()
    request_date = models.DateField()
    count = models.IntegerField()
    objects = PasswordResetAuditManager()

    class Meta:
        ordering = ["-modified"]
        verbose_name = "Password reset audit"
        verbose_name_plural = "Password reset audit"

    def __str__(self):
        return "{} {} {}".format(self.email, self.request_date, self.count)
