# -*- encoding: utf-8 -*-
import os

from django.core.exceptions import ImproperlyConfigured
from django.urls import reverse_lazy


def get_env_variable(key):
    """
    Get the environment variable or return exception
    Copied from Django two scoops book
    """
    try:
        return os.environ[key]
    except KeyError:
        error_msg = "Set the {} env variable".format(key)
        print("ImproperlyConfigured: {}".format(error_msg))
        raise ImproperlyConfigured(error_msg)


def get_env_variable_bool(key):
    """
    Retrieves env vars and makes Python boolean replacements
    Copied from http://www.wellfireinteractive.com/blog/easier-12-factor-django/
    """
    result = get_env_variable(key)
    if result == "True":
        result = True
    elif result == "False":
        result = False
    else:
        error_msg = (
            "The {} variable must be set to 'True' or 'False': {}".format(
                key, result
            )
        )
        print("ImproperlyConfigured: {}".format(error_msg))
        raise ImproperlyConfigured(error_msg)
    return result


DEBUG = True
TESTING = True

SESSION_COOKIE_SECURE = False
CSRF_COOKIE_SECURE = False

# We use the 'SITE_NAME' for the name of the database and the name of the
# cloud files container.
SITE_NAME = "app_login"

ADMINS = (("admin", "code@pkimber.net"),)

MANAGERS = ADMINS

# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# In a Windows environment this must be set to your system time zone.
TIME_ZONE = "Europe/London"

# Language code for this installation. All choices can be found here:
# http://www.i18nguy.com/unicode/language-identifiers.html
LANGUAGE_CODE = "en-gb"

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = True

# If you set this to False, Django will not format dates, numbers and
# calendars according to the current locale.
USE_L10N = True

# If you set this to False, Django will not use timezone-aware datetimes.
USE_TZ = True

# Absolute filesystem path to the directory that will hold user-uploaded files.
# Example: "/home/media/media.lawrence.com/media/"
MEDIA_ROOT = ""

# URL that handles the media served from MEDIA_ROOT. Make sure to use a
# trailing slash.
# Examples: "http://media.lawrence.com/media/", "http://example.com/media/"
MEDIA_URL = ""

# Absolute path to the directory static files should be collected to.
# Don't put anything in this directory yourself; store your static files
# in apps' "static/" subdirectories and in STATICFILES_DIRS.
# Example: "/home/media/media.lawrence.com/static/"
STATIC_ROOT = ""

# URL prefix for static files.
# Example: "http://media.lawrence.com/static/"
STATIC_URL = "/static/"

# Additional locations of static files
STATICFILES_DIRS = (
    # Put strings here, like "/home/html/static" or "C:/www/django/static".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
)

# List of finder classes that know how to find static files in
# various locations.
STATICFILES_FINDERS = (
    "django.contrib.staticfiles.finders.FileSystemFinder",
    "django.contrib.staticfiles.finders.AppDirectoriesFinder",
    # 'django.contrib.staticfiles.finders.DefaultStorageFinder',
)

# Make this unique, and don't share it with anybody.
SECRET_KEY = "#9x3dk(nl82sihl7c^u_#--yp((!g2ehd_1pmp)fpgx=h9(l9="

# ref AXES Cache problems
# https://github.com/jazzband/django-axes/blob/master/docs/configuration.rst#cache-problems
CACHES = {"default": {"BACKEND": "django.core.cache.backends.dummy.DummyCache"}}

MIDDLEWARE = (
    "django.contrib.sessions.middleware.SessionMiddleware",
    "django.middleware.common.CommonMiddleware",
    "django.middleware.csrf.CsrfViewMiddleware",
    "django.contrib.auth.middleware.AuthenticationMiddleware",
    "django.contrib.auth.middleware.PersistentRemoteUserMiddleware",
    "django.contrib.messages.middleware.MessageMiddleware",
    "django.middleware.clickjacking.XFrameOptionsMiddleware",
    "reversion.middleware.RevisionMiddleware",
)

ROOT_URLCONF = "example_login.urls"

# Python dotted path to the WSGI application used by Django's runserver.
WSGI_APPLICATION = "example_login.wsgi.application"

TEMPLATES = [
    {
        "BACKEND": "django.template.backends.django.DjangoTemplates",
        "DIRS": [],
        "APP_DIRS": True,
        "OPTIONS": {
            "context_processors": [
                "django.contrib.auth.context_processors.auth",
                "django.template.context_processors.debug",
                "django.template.context_processors.i18n",
                "django.template.context_processors.media",
                "django.template.context_processors.request",
                "django.template.context_processors.static",
                "django.template.context_processors.tz",
                "django.contrib.messages.context_processors.messages",
            ],
            "string_if_invalid": "**** INVALID EXPRESSION: %s ****",
        },
    }
]

DJANGO_APPS = (
    "django.contrib.auth",
    "django.contrib.contenttypes",
    "django.contrib.sessions",
    "django.contrib.messages",
    "django.contrib.staticfiles",
)

THIRD_PARTY_APPS = (
    # add django_dramatiq to installed apps before any of your custom apps
    "django_dramatiq",
    "django_recaptcha",
    "mozilla_django_oidc",
    "rest_framework",
    # http://www.django-rest-framework.org/api-guide/authentication#tokenauthentication
    "rest_framework.authtoken",
    "reversion",
    "taggit",
)

LOCAL_APPS = ("api", "base", "contact", "example_login", "login", "mail")

INSTALLED_APPS = DJANGO_APPS + THIRD_PARTY_APPS + LOCAL_APPS

LOGGING = {
    "version": 1,
    "disable_existing_loggers": True,
    "formatters": {
        "standard": {
            "format": "[%(asctime)s] %(levelname)s [%(name)s:%(lineno)s] %(message)s",
            "datefmt": "%d/%b/%Y %H:%M:%S",
        }
    },
    "handlers": {
        "logfile": {
            "level": "DEBUG",
            "class": "logging.handlers.RotatingFileHandler",
            "filename": os.path.join(
                get_env_variable("LOG_FOLDER"),
                "{}-{}-logger.log".format(
                    get_env_variable("DOMAIN").replace("_", "-"),
                    get_env_variable("LOG_SUFFIX"),
                ),
            ),
            "maxBytes": 500000,
            "backupCount": 10,
            "formatter": "standard",
        },
        "console": {
            "level": "INFO",
            "class": "logging.StreamHandler",
            "formatter": "standard",
        },
    },
    "loggers": {
        "django": {"handlers": ["console"], "propagate": True, "level": "WARN"},
        "django.db.backends": {
            "handlers": ["console"],
            "level": "DEBUG",
            "propagate": False,
        },
        "mozilla_django_oidc": {"handlers": ["console"], "level": "DEBUG"},
        "": {"handlers": ["logfile"], "level": "DEBUG"},
    },
}

AUTH_PASSWORD_VALIDATORS = [
    {
        "NAME": "django.contrib.auth.password_validation.UserAttributeSimilarityValidator"
    },
    {
        "NAME": "django.contrib.auth.password_validation.MinimumLengthValidator",
        "OPTIONS": {"min_length": 8},
    },
]

AUTHENTICATION_BACKENDS = (
    "login.auth.ExistingRemoteUserBackend",
    "django.contrib.auth.backends.ModelBackend",
)

# https://mozilla-django-oidc.readthedocs.io/
USE_OPENID_CONNECT = get_env_variable_bool("USE_OPENID_CONNECT")
if USE_OPENID_CONNECT:
    AUTHENTICATION_BACKENDS += (
        "login.service.KBSoftwareOIDCAuthenticationBackend",
    )
    LOGIN_REDIRECT_URL_FAILURE = reverse_lazy("login")
    # set -x OIDC_AUTHENTICATION_CALLBACK_URL "http://localhost:8000/oidc/callback/"
    # OIDC_AUTHENTICATION_CALLBACK_URL = reverse_lazy("oidc_authentication_callback")
    OIDC_CREATE_USER = False
    OIDC_OP_AUTHORIZATION_ENDPOINT = get_env_variable(
        "OIDC_OP_AUTHORIZATION_ENDPOINT"
    )
    OIDC_OP_JWKS_ENDPOINT = get_env_variable("OIDC_OP_JWKS_ENDPOINT")
    OIDC_OP_TOKEN_ENDPOINT = get_env_variable("OIDC_OP_TOKEN_ENDPOINT")
    OIDC_OP_USER_ENDPOINT = "NOT_USED_BY_KB_LOGIN_SERVICE"
    OIDC_RP_CLIENT_ID = get_env_variable("OIDC_RP_CLIENT_ID")
    OIDC_RP_CLIENT_SECRET = get_env_variable("OIDC_RP_CLIENT_SECRET")
    OIDC_RP_SIGN_ALGO = get_env_variable("OIDC_RP_SIGN_ALGO")
    OIDC_USE_NONCE = get_env_variable_bool("OIDC_USE_NONCE")
    KB_TEST_EMAIL_FOR_OIDC = get_env_variable("KB_TEST_EMAIL_FOR_OIDC")
    KB_TEST_EMAIL_USERNAME = get_env_variable("KB_TEST_EMAIL_USERNAME")
    # OIDC_RP_IDP_SIGN_KEY = "Patrick"
    # "https://login.microsoftonline.com/{tenant}/discovery/v2.0/keys",
    #    "https://login.microsoftonline.com/common/discovery/v2.0/keys"
    # )
    # OIDC_VERIFY_JWT = get_env_variable_bool("OIDC_VERIFY_JWT")
    # OIDC_VERIFY_SSL = get_env_variable_bool("OIDC_VERIFY_SSL")
    #
    # The 'OPEN_ID_CONNECT_EMBER_REDIRECT_' settings need to match the settings
    # used by the Ember login or the Django login will be rejected.
    OPEN_ID_CONNECT_EMBER_REDIRECT_URI = "http://localhost:4200"
    OPEN_ID_CONNECT_EMBER_REDIRECT_ROUTE = "login"

# from datetime import timedelta
#
# AXES_COOLOFF_TIME = timedelta(minutes=15)
# AXES_FAILURE_LIMIT = 5
# AXES_LOCKOUT_TEMPLATE = ("login/axes_lockout_template.html",)
# AXES_PASSWORD_FORM_FIELD = "password1"

# See the list of constants at the top of 'mail.models'
MAIL_TEMPLATE_TYPE = "django"

DEFAULT_AUTO_FIELD = "django.db.models.AutoField"

DEFAULT_FROM_EMAIL = "notify@pkimber.net"
# mandrill
# EMAIL_BACKEND = 'djrill.mail.backends.djrill.DjrillBackend'
# MANDRILL_API_KEY = get_env_variable('MANDRILL_API_KEY')
# MANDRILL_USER_NAME = get_env_variable('MANDRILL_USER_NAME')

EMAIL_BACKEND = "django.core.mail.backends.console.EmailBackend"

FTP_STATIC_DIR = None
FTP_STATIC_URL = None

# URL where requests are redirected after login when the contrib.auth.login
# view gets no next parameter.
LOGIN_REDIRECT_URL = reverse_lazy("project.dash")
LOGOUT_REDIRECT_URL = reverse_lazy("project.home")

SENDFILE_BACKEND = "sendfile.backends.development"
SENDFILE_ROOT = "media-private"

SILENCED_SYSTEM_CHECKS = ["captcha.recaptcha_test_key_error"]

USE_OPENID_CONNECT = get_env_variable_bool("USE_OPENID_CONNECT")
