# -*- encoding: utf-8 -*-
from django.contrib.auth.models import User, Group
from django.contrib.auth.mixins import AccessMixin

from django.urls import reverse
from django.views.generic import TemplateView

from base.view_utils import BaseMixin
from braces.views import LoginRequiredMixin
from login.views import (
    RoleRequiredMixin,
    UpdateUserNameView,
    UpdateUserPasswordView,
)


class CheckerRoleRequiredMixin(RoleRequiredMixin):
    def get_role(self):
        # simulate role in a settings table
        try:
            role = Group.objects.get(name="checker")
        except Group.DoesNotExist:
            role = None
        return role


class BasicCheckerRoleRequiredMixin(CheckerRoleRequiredMixin):
    STAFF_REQUIRED = False


class MyUpdateUserNameView(UpdateUserNameView):
    def get_success_url(self):
        return reverse("example.test")


class MyUpdateUserPasswordView(UpdateUserPasswordView):
    def get_success_url(self):
        return reverse("example.test")


class NoAuthView(BaseMixin, TemplateView):
    template_name = "example/no_auth.html"


class SettingsView(BaseMixin, TemplateView):
    template_name = "example/settings.html"


class TestView(LoginRequiredMixin, BaseMixin, TemplateView):
    template_name = "example/test.html"

    def get_context_data(self, **kwargs):
        context = super(TestView, self).get_context_data(**kwargs)
        if self.request.user.is_staff:
            context.update(
                dict(request_user=self.request.user, users=User.objects.all())
            )
        return context


class CheckerView(
    AccessMixin, CheckerRoleRequiredMixin, BaseMixin, TemplateView
):
    template_name = "example/test.html"


class BasicCheckerView(
    AccessMixin, BasicCheckerRoleRequiredMixin, BaseMixin, TemplateView
):
    template_name = "example/test.html"
