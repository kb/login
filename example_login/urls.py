# -*- encoding: utf-8 -*-
from django.conf import settings
from django.urls import include, re_path
from django.urls import path
from django.views.generic import TemplateView

from login.api import ObtainAuthTokenOnCode
from login.views import RegisterCreateView
from .views import (
    BasicCheckerView,
    CheckerView,
    MyUpdateUserNameView,
    MyUpdateUserPasswordView,
    NoAuthView,
    SettingsView,
    TestView,
)


urlpatterns = [
    re_path(r"^", view=include("login.urls")),
    re_path(
        r"^$",
        view=TemplateView.as_view(template_name="example/home.html"),
        name="project.home",
    ),
    re_path(
        r"^dash/$",
        view=TemplateView.as_view(template_name="example/dash.html"),
        name="project.dash",
    ),
    re_path(r"^oidc/", view=include("mozilla_django_oidc.urls")),
    re_path(
        r"^settings/$",
        view=SettingsView.as_view(),
        name="project.settings",
    ),
    re_path(r"^test/$", view=TestView.as_view(), name="example.test"),
    re_path(
        r"^token/$",
        view=ObtainAuthTokenOnCode.as_view(),
        name="api.token.auth",
    ),
    re_path(r"^checker/$", view=CheckerView.as_view(), name="example.checker"),
    re_path(
        r"^checker/basic/$",
        view=BasicCheckerView.as_view(),
        name="example.checker.basic",
    ),
    re_path(
        r"^accounts/register/$",
        view=RegisterCreateView.as_view(),
        name="register",
    ),
    re_path(
        r"^accounts/user/(?P<pk>\d+)/username/$",
        view=MyUpdateUserNameView.as_view(),
        name="update_user_name",
    ),
    re_path(
        r"^accounts/user/(?P<pk>\d+)/password/$",
        view=MyUpdateUserPasswordView.as_view(),
        name="update_user_password",
    ),
    re_path(r"^no/auth/$", view=NoAuthView.as_view(), name="project.no.auth"),
]

if settings.DEBUG:
    import debug_toolbar

    urlpatterns = [
        re_path(r"^__debug__/", include(debug_toolbar.urls))
    ] + urlpatterns
