# -*- encoding: utf-8 -*-
import pytest

from django.contrib.auth.models import User
from django.test import TestCase
from django.urls import reverse
from django.utils import timezone

from login.tests.factories import TEST_PASSWORD
from login.tests.scenario import (
    default_scenario_login,
    get_user_staff,
    get_user_web,
)


@pytest.mark.django_db
def test_register(client):
    response = client.get(reverse("register"))
    assert 200 == response.status_code


@pytest.mark.django_db
def test_register_post(client):
    data = {
        "username": "patrick",
        "password1": "my-password",
        "password2": "my-password",
    }
    response = client.post(reverse("register"), data)
    assert 302 == response.status_code, response.context["form"].errors
    assert reverse("project.dash") == response.url
    user = User.objects.get(username="patrick")
    assert "" == user.email
    assert user.is_active is True
    assert user.is_staff is False
    assert user.is_superuser is False
    assert timezone.now().date() == user.date_joined.date()
    assert timezone.now().date() == user.last_login.date()


class TestUpdateUserPassword(TestCase):
    def setUp(self):
        default_scenario_login()
        staff = get_user_staff()
        self.assertTrue(
            self.client.login(username=staff.username, password=TEST_PASSWORD)
        )

    def test_update_user_name(self):
        user = get_user_web()
        url = reverse("update_user_name", kwargs=dict(pk=user.pk))
        self.assertEqual("web", user.username)
        self.client.post(url, dict(username="webber"))
        user = User.objects.get(username="webber")
        self.assertEqual("webber", user.username)

    def test_update_user_password(self):
        user = get_user_web()
        url = reverse("update_user_password", kwargs=dict(pk=user.pk))
        response = self.client.post(
            url, dict(new_password1="12345678", new_password2="12345678")
        )
        self.assertEqual(response.status_code, 302)
