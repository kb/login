# -*- encoding: utf-8 -*-
import pytest

from django.urls import reverse
from http import HTTPStatus

from login.tests.factories import TEST_PASSWORD, UserFactory, GroupFactory


@pytest.mark.django_db
def test_checker_role_required_no_role_defined(client):
    """Allow access if no role defined"""
    user = UserFactory(username="staff", is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD)
    response = client.get(reverse("example.checker"))
    assert HTTPStatus.OK == response.status_code


@pytest.mark.django_db
def test_checker_role_required_not_logged_in(client):
    """Redirect to login if user not logged in."""
    GroupFactory(name="checker")
    response = client.get(reverse("example.checker"))
    assert HTTPStatus.FOUND == response.status_code


@pytest.mark.django_db
def test_checker_role_required_no_role_denied(client):
    """Permission denied if user does not have role."""
    GroupFactory(name="checker")
    user = UserFactory(username="staff", is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD)
    response = client.get(reverse("example.checker"))
    assert HTTPStatus.FORBIDDEN == response.status_code


@pytest.mark.django_db
def test_checker_role_required_no_staff_denied(client):
    """Permission denied if user is not staff."""
    checker_role = GroupFactory(name="checker")
    user = UserFactory(username="web")
    checker_role.user_set.add(user)
    assert client.login(username=user.username, password=TEST_PASSWORD)
    assert user.is_authenticated
    response = client.get(reverse("example.checker"))
    assert HTTPStatus.FORBIDDEN == response.status_code


@pytest.mark.django_db
def test_basic_checker_role_required_no_staff_allowed(client):
    """Permission denied if user is not staff"""
    checker_role = GroupFactory(name="checker")

    user = UserFactory(username="web")
    checker_role.user_set.add(user)
    assert client.login(username=user.username, password=TEST_PASSWORD)
    assert user.is_authenticated
    response = client.get(reverse("example.checker.basic"))
    assert HTTPStatus.OK == response.status_code


@pytest.mark.django_db
def test_checker_role_required_has_role_allowed(client):
    """Allow access user has the design role"""
    checker_role = GroupFactory(name="checker")

    user = UserFactory(username="staff", is_staff=True)
    checker_role.user_set.add(user)
    assert client.login(username=user.username, password=TEST_PASSWORD)
    response = client.get(reverse("example.checker"))
    assert HTTPStatus.OK == response.status_code


@pytest.mark.django_db
def test_checker_role_required_superuser_allowed(client):
    """Allow access if user is superuser (and staff) regardless of whether has
    role
    """
    GroupFactory(name="checker")
    user = UserFactory(username="admin", is_staff=True, is_superuser=True)
    assert client.login(username=user.username, password=TEST_PASSWORD)
    response = client.get(reverse("example.checker"))
    assert HTTPStatus.OK == response.status_code
