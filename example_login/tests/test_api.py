# -*- encoding: utf-8 -*-
import json
import pytest
import responses

from django.conf import settings
from django.urls import reverse
from http import HTTPStatus
from rest_framework.authtoken.models import Token
from rest_framework.exceptions import ErrorDetail
from unittest import mock

from api.tests.fixture import api_client
from contact.tests.factories import ContactFactory
from login.tests.factories import UserFactory


@pytest.mark.django_db
@responses.activate
def test_api_token_auth(api_client):
    contact = ContactFactory(
        user=UserFactory(username="patrick", email="patrick@kbsoftware.co.uk")
    )
    with pytest.raises(Token.DoesNotExist):
        Token.objects.get(user=contact.user)
    responses.add(
        responses.GET,
        settings.OIDC_OP_JWKS_ENDPOINT,
        json={},
        status=HTTPStatus.OK,
    )
    responses.add(
        responses.GET,
        settings.OIDC_OP_TOKEN_ENDPOINT,
        json={},
        status=HTTPStatus.OK,
    )
    responses.add(
        responses.POST,
        settings.OIDC_OP_TOKEN_ENDPOINT,
        json={},
        status=HTTPStatus.OK,
    )
    with mock.patch(
        "mozilla_django_oidc.auth.OIDCAuthenticationBackend.verify_token"
    ) as token_mock:
        token_mock.return_value = {"email": "patrick@kbsoftware.co.uk"}
        response = api_client.post(
            reverse("api.token.auth"),
            json.dumps({"code": "A1B2C3D4"}),
            content_type="application/json",
        )
    assert HTTPStatus.OK == response.status_code, response.data
    token = Token.objects.get(user=contact.user)
    assert {"token": token.key, "contact_id": contact.pk} == response.data


@pytest.mark.django_db
@responses.activate
def test_api_token_auth_no_contact(api_client):
    user = UserFactory(username="patrick", email="patrick@kbsoftware.co.uk")
    with pytest.raises(Token.DoesNotExist):
        Token.objects.get(user=user)
    responses.add(
        responses.GET,
        settings.OIDC_OP_JWKS_ENDPOINT,
        json={},
        status=HTTPStatus.OK,
    )
    responses.add(
        responses.GET,
        settings.OIDC_OP_TOKEN_ENDPOINT,
        json={},
        status=HTTPStatus.OK,
    )
    responses.add(
        responses.POST,
        settings.OIDC_OP_TOKEN_ENDPOINT,
        json={},
        status=HTTPStatus.OK,
    )
    with mock.patch(
        "mozilla_django_oidc.auth.OIDCAuthenticationBackend.verify_token"
    ) as token_mock:
        token_mock.return_value = {"email": "patrick@kbsoftware.co.uk"}
        response = api_client.post(
            reverse("api.token.auth"),
            json.dumps({"code": "A1B2C3D4"}),
            content_type="application/json",
        )
    assert HTTPStatus.FORBIDDEN == response.status_code, response.data
    assert {
        "detail": ErrorDetail(
            string="User 'patrick' does not have a 'Contact'",
            code="authentication_failed",
        )
    } == response.data
    # 05/05/2022, Looking at the code, I would expect the ``Token`` to have
    # been created for the user?
    # with pytest.raises(Token.DoesNotExist):
    Token.objects.get(user=user)
